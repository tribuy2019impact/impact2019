# TriBuy

TriBuy is an e-commerce platform that promotes and sells authentic indigenous products. The platform envisions to provide an accessible and comfortable way of buying authentic indigenous products that in return give our indigenous people a chance to showcase their cultural heritage and also earn at the same time.

